Cnek
====

Snake in C using curses.

Acknowlegdements
-----------------

Code style / structure inspired by Stephen Brennan at https://github.com/brenns10

License
-------

![GNU GPLv3 logo][1]

This project is under the GNU GPLv3 license.  
See the [LICENSE][2] file for details.

[1]: https://www.gnu.org/graphics/gplv3-127x51.png
[2]: LICENSE
