EXE 		= cnek

SRCDIR		= src
OBJDIR		= obj
OUTDIR 		= bin
DIRS		= $(SRCDIR) $(OBJDIR) $(OUTDIR)

CFILES		= $(SRCDIR)/main.c $(SRCDIR)/snek.c $(SRCDIR)/util.c
SRCFILES	= $(CFILES)
OBJECTS		= $(patsubst %.c,%.o,$(patsubst $(SRCDIR)%,$(OBJDIR)%,$(SRCFILES)))

LDFLAGS 	= -lncurses

$(EXE): $(OBJECTS) $(DIRS)
	cc -o $(OUTDIR)/$(EXE) $(OBJECTS) $(LDFLAGS)

$(OBJECTS) : $(SRCFILES) $(DIRS)
	cc -c $(SRCFILES)
	mv *.o $(OBJDIR)

$(DIRS) :
	mkdir -p $(DIRS)

clean :
	rm $(OUTDIR)/$(EXE) $(OBJECTS)
