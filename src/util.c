/************************************************
 *
 * @file        util.c
 *
 * @author      Cornelius Sevald-Krause
 *
 * @date        Created Thursday, 7 March 2019
 *
 * @license     GNU GPLv3
 *
************************************************/

#include <time.h>
#include "util.h"

void sleep_milli (int milliseconds) {
        struct timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = milliseconds * 1000 * 1000;
        nanosleep(&ts, NULL);
}
