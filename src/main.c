/************************************************
 *
 * @file        main.c
 *
 * @author      Cornelius Sevald-Krause
 *
 * @date        Created Tuesday, 26 February 2019
 *
 * @license     GNU GPLv3
 *
************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <ncurses.h>
#include "util.h"
#include "snek.h"

#define PROGRAM_NAME "cnek"
#define AUTHORS \
        "Cornelius Sevald-Krause"
#define VERSION "1.0.0"

/* 2 columns per cell looks a lot nicer. */
#define COLS_PER_CELL 2

/* Contraint values. */
#define MIN_BOARD_WIDTH 2
#define MIN_BOARD_HEIGTH 2
#define MAX_BOARD_WIDTH INT_MAX
#define MAX_BOARD_WIDTH INT_MAX
#define MAX_WAIT 500
#define MIN_WAIT 50

/* The default parameters. */
#define DEFAULT_BOARD_WIDTH 17
#define DEFAULT_BOARD_HEIGHT 17
#define DEFAULT_WC_TYPE WC_TELEPORT

/*
 * The size of the board window.
 * The extra 2 cells is for the border.
 */
#define BOARD_ROWS board_height + 2
#define BOARD_COLS board_width * COLS_PER_CELL + 2

/*
 * The size of the score window.
 */
#define SCORE_ROWS 2 + 2
#define SCORE_COLS 12 + 2

/*
 * Pausing.
 */
#define PAUSE_ROWS 3 + 2
#define PAUSE_COLS 8 + 2
#define PM_ENTRY_COUNT 3

#define SC_EMPTY_STR " "
#define SC_SEGMENT_STR "#"
#define SC_FRUIT_STR "*"

/*
 * Initialization & de-initialization functions.
 */
static void init_colors ();
static void init_curses ();
static void de_init_curses ();
/*
 * Display functions.
 */
static struct cell_location board_to_display (struct cell_location loc);
static void print_cell (WINDOW *board_win, int row, int col, char *cell_str);
void display (WINDOW *board_win, WINDOW *score_win, struct snek_game *game);
static void display_board (WINDOW *board_win, struct snek_game *game);
static void display_score (WINDOW *score_win, struct snek_game *game);
static int pause (WINDOW *pause_win);
static void display_help (WINDOW *help_win);
/*
 * Time.
 */
static float get_wait_time (struct snek_game *game); // Get the wait time in milliseconds.
/*
 * Helper functions.
 */
static int parse_args (int argc, char *argv[]);
static void print_help ();

/*
 * File-wide variables.
 */
const char *progname;
enum wall_collisions wc_type = DEFAULT_WC_TYPE;
int board_width = DEFAULT_BOARD_WIDTH;
int board_height = DEFAULT_BOARD_HEIGHT;

static void init_curses () {
        initscr();              // initialize curses
        cbreak();               // pass key presses to program, but not signals
        noecho();               // don't echo key presses to screen
        keypad(stdscr, TRUE);   // allow arrow keys
        timeout(0);             // no blocking on getch()
        curs_set(0);            // set the cursor to invisible
        init_colors();
}

static void init_colors () {
        start_color();

        init_pair(SC_SEGMENT, COLOR_GREEN, COLOR_BLACK);
        init_pair(SC_FRUIT, COLOR_RED, COLOR_BLACK);
}

static void de_init_curses () {
        echo();                 // Echo key presses again.
        curs_set(1);            // set the cursor back to normal
        wclear(stdscr);
        endwin();
}

static struct cell_location board_to_display (struct cell_location loc) {
        struct cell_location disp_loc;
        disp_loc.row = BOARD_ROWS - loc.row - 2;
        disp_loc.col = loc.col * COLS_PER_CELL + 1;

        return disp_loc;
}

static void print_cell (WINDOW *board_win, int row, int col, char *cell_str) {
        for (int i = 0; i < COLS_PER_CELL; i++) {
                wmove(board_win, row, col + i);
                waddstr(board_win, cell_str);
        }
}

void display (WINDOW *board_win, WINDOW *score_win, struct snek_game *game) {
        display_board (board_win, game);
        display_score (score_win, game);
}

static void display_board (WINDOW *board_win, struct snek_game *game) {
        /* Clear the old board. */
        wclear(board_win);
        box(board_win, 0, 0);

        /* Draw the snake. */
        struct snek *snek = game->snek;
        if (snek) {
                wattron(board_win, COLOR_PAIR(SC_SEGMENT));
                for (
                        struct snekment *current = snek->head;
                        current;
                        current = current->prev
                ) {
                        struct cell_location disp_loc =
                                board_to_display(current->loc);
                        print_cell (board_win, disp_loc.row, disp_loc.col, SC_SEGMENT_STR);
                }
                wattroff(board_win, COLOR_PAIR(SC_SEGMENT));
        }

        /* Draw the fruit. */
        struct fruit *fruit = game->current_fruit;
        if (fruit) {
                struct cell_location disp_loc =
                        board_to_display(fruit->loc);

                wattron(board_win, COLOR_PAIR(SC_FRUIT));
                print_cell(board_win, disp_loc.row, disp_loc.col, SC_FRUIT_STR);
                wattroff(board_win, COLOR_PAIR(SC_FRUIT));
        }
        wrefresh(board_win);
}

static void display_score (WINDOW *score_win, struct snek_game *game) {
        /* Clea the old score. */
        wclear(score_win);
        box(score_win, 0, 0);

        int row = 1;
        int col = 1;
        size_t n = SCORE_COLS - 2;
        char fruit_score_str[n];
        char time_score_str[n];

        snprintf(fruit_score_str, n, "Score: %d", game->fruit_score);
        snprintf(time_score_str, n, "Time:  %d", game->tick_score);

        mvwaddnstr(score_win, row++, col, fruit_score_str, n);
        mvwaddnstr(score_win, row++, col, time_score_str, n);

        wrefresh(score_win);
}

static void display_help (WINDOW *help_win) {
        wtimeout(help_win, -1);
        keypad(help_win, TRUE);
        wclear(help_win);

        wprintw(help_win, "Cnek game help.\n\n");
        wprintw(help_win, "CONTROLS:\n");
        wprintw(
                help_win,
                "   Turn right:         'd' or right arrow key.\n"
                "   Turn upwards:       'w' or up arrow key.\n"
                "   Turn left:          'a' or left arrow key.\n"
                "   Turn downwards:     's' or down arrow key.\n"
                "   Pause               'p'.\n"
                "   Quit                'q' or <ESC>.\n"
        );

        wattron(help_win, A_REVERSE);
        mvwprintw(help_win, LINES-1, 0, "Press <ESC> to return.");
        wattroff(help_win, A_REVERSE);
        while (1) {
                if(wgetch(help_win) == 27) {
                        break;
                }
        }
        timeout(0);
        wclear(help_win);
}

static int pause (WINDOW *pause_win) {
        wtimeout(pause_win, -1);
        keypad(pause_win, TRUE);

        int row = 1;
        int col = 1;
        size_t n = PAUSE_COLS - 2;
        char *menu_entries[PM_ENTRY_COUNT] = {
                "Resume",
                "Quit",
                "Help"
        };
        int menu_index = 0;
        int should_quit = 0;
        int pausing = 1;

        while (pausing) {
                wclear(pause_win);
                box(pause_win, 0, 0);
                for (int i = 0; i < PM_ENTRY_COUNT; i++) {
                        if (i == menu_index) {
                                wattron(pause_win, A_REVERSE);
                        }
                        char *entry = menu_entries[i];
                        mvwaddnstr(
                                pause_win,
                                row + i, col + (PAUSE_COLS - 2 - strlen(entry)) / 2,
                                entry, n
                        );
                        if (i == menu_index) {
                                wattroff(pause_win, A_REVERSE);
                        }
                }
                int input = wgetch(pause_win);
                if (input == KEY_UP || input == 'w') {
                        if (--menu_index < 0) {
                                menu_index += PM_ENTRY_COUNT;
                        }
                } else if (input == KEY_DOWN || input == 's') {
                        if (++menu_index >= PM_ENTRY_COUNT) {
                                menu_index -= PM_ENTRY_COUNT;
                        }
                } else if (input == KEY_ENTER || input == '\n') {
                        if (menu_index == 2) {
                                display_help(stdscr);
                        }
                        /* Un-pause if the index is 0 (Resume) or 1 (quit) */
                        pausing = !(menu_index == 0 || menu_index == 1 || menu_index == 2);
                        /* Quit if the menu index is 1 (quit). */
                        should_quit = menu_index == 1;
                }
        }
        timeout(0);
        wclear(pause_win);
        return should_quit;
}

static float get_wait_time (struct snek_game *game) {
        /* Calculate the game speed based off of the fruit score. */
        return MAX_WAIT / (game->fruit_score + 1) + MIN_WAIT;
}

static void print_help () {

        char *help_str =
               ("Usage: %s [OPTIONS]\n\n"
                "Options:\n"
                "   -w, --width N               Use a specified board width equal to N\n"
                "   -h, --height N              Use a specified board heigth equal to N\n"
                "   -c, --collision-type T      Determines if the snake should die (T=%d) "
                "or teleport to the other side (T=%d) when colliding with a wall\n"
                "   --help                      Print this message and exit\n"
                "   --version                   Print the version information and exit\n");
        printf(help_str, progname, WC_DIE, WC_TELEPORT);
}

static void print_version () {
        char *version_str =
               ("%s version %s\n"
                "License GNU GPL version 3 <https://www.gnu.org/licenses/gpl.html>.\n"
                "\n"
                "Written by %s.\n");
        printf(version_str, PROGRAM_NAME, VERSION, AUTHORS);
}

static int parse_args (int argc, char *argv[]) {

        char *valid_args[] = {
                "--help", "--version",
                "--collision-type", "-c",
                "--width", "-w",
                "--height", "-h"
        };

        for (int i = 1; i < argc; i++) {
                /* Test for help. */
                if (
                        !strcmp(valid_args[0], argv[i])
                ) {
                        print_help();
                        return 0;
                }
                /* Test for version request. */
                if (
                        !strcmp(valid_args[1], argv[i])
                ) {
                        print_version();
                        return 0;
                }
                /* Test for specified collision type. */
                else if (
                        !strcmp(valid_args[2], argv[i]) ||
                        !strcmp(valid_args[3], argv[i])
                ) {
                        /* Check if a number is specified after the argument. */
                        if (i + 1 >= argc) {
                                printf("%s: missing value for collision type \n", argv[0]);
                                printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                                return 1;
                        }
                        /* Check if the following arg is invalid. */
                        char *endptr;
                        /* Note the ++i */
                        int val = strtol(argv[++i], &endptr, 10);
                        if (
                                *endptr != '\0' ||
                                !(val == WC_DIE ||
                                val == WC_TELEPORT)
                        ) {
                                printf("Incorrect argument '%s' for collision type\n", argv[i]);
                                printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                                return 1;
                        }
                        wc_type = val;
                }
                /* Test for specified board width. */
                else if (
                        !strcmp(valid_args[4], argv[i]) ||
                        !strcmp(valid_args[5], argv[i])
                ) {
                        /* Check if a number is specified after the argument. */
                        if (i + 1 >= argc) {
                                printf("%s: missing value for board width\n", argv[0]);
                                printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                                return 1;
                        }
                        /* Check if the following arg is invalid. */
                        char *endptr;
                        /* Note the ++i */
                        int val = strtol(argv[++i], &endptr, 10);
                        if (
                                *endptr != '\0' ||
                                val < MIN_BOARD_WIDTH ||
                                val > MAX_BOARD_WIDTH
                        ) {
                                printf("Incorrect argument '%s' for board width\n", argv[i]);
                                printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                                return 1;
                        }
                        board_width = val;
                }
                /* Test for specified board height. */
                else if (
                        !strcmp(valid_args[6], argv[i]) ||
                        !strcmp(valid_args[7], argv[i])
                ) {
                        /* Check if a number is specified after the argument. */
                        if (i + 1 >= argc) {
                                printf("%s: missing value for board height\n", argv[0]);
                                printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                                return 1;
                        }
                        /* Check if the following arg is invalid. */
                        char *endptr;
                        /* Note the ++i */
                        int val = strtol(argv[++i], &endptr, 10);
                        if (
                                *endptr != '\0' ||
                                val < MIN_BOARD_WIDTH ||
                                val > MAX_BOARD_WIDTH
                        ) {
                                printf("Incorrect argument '%s' for board height\n", argv[i]);
                                printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                                return 1;
                        }
                        board_height = val;
                }
                /* Invalid argument specified. */
                else {
                        printf("Invalid argument '%s'\n", argv[i]);
                        printf("Try '%s %s' for more info.\n", argv[0], valid_args[0]);
                        return 1;
                }
        }
        return -1;
}

int main (int argc, char *argv[]) {
        /*
         * Parse the arguments.
         * A return value of 0 means the program should exit succsessfully.
         * A return value of 1 means the program should exit with a failure.
         * Any other return value means the program should continue.
         */
        progname = argv[0];
        int parse_result = parse_args(argc, argv);
        if (parse_result == 0) {
                return 0;
        } else if (parse_result == 1) {
                return 1;
        }

        /* Initialize random seed. */
        srand((unsigned int)time(NULL));

        struct snek_game *game;
        enum snek_move move = SM_NONE;
        int tick = 0;

        WINDOW *board_win, *score_win, *pause_win;
        game = sg_create(board_height, board_width, wc_type);

        /* Initialize curses. */
        init_curses();
        init_colors();

        /* Create the windows. */
        board_win = newwin(
                BOARD_ROWS, BOARD_COLS,
                (LINES - BOARD_ROWS - SCORE_ROWS) / 2,
                (COLS - BOARD_COLS - SCORE_COLS) / 2
        );
        score_win = newwin(
                SCORE_ROWS, SCORE_COLS,
                (LINES - BOARD_ROWS - SCORE_ROWS) / 2,
                (COLS - BOARD_COLS - SCORE_COLS) / 2 + BOARD_COLS
        );
        pause_win = newwin(
                PAUSE_ROWS, PAUSE_COLS,
                (LINES - SCORE_ROWS - PAUSE_ROWS) / 2,
                (COLS - SCORE_COLS - PAUSE_COLS) / 2
        );
        wrefresh(stdscr);

        /* Test display the game running. */
        while (game->state == GS_RUNNING) {
                int wait = get_wait_time(game);

                sg_tick(game, move);
                tick++;

                display(board_win, score_win, game);
                doupdate();
                sleep_milli(wait);

                switch (getch()) {
                        case 'q':
                                game->state = GS_LOST;
                                break;
                        case 27: // The escape key
                                game->state = GS_LOST;
                                break;
                        case 'p':
                                if (pause(pause_win)) {
                                        game->state = GS_LOST;
                                }
                                break;
                        /* Directional input. */
                        case KEY_RIGHT:
                                move = SM_RIGHT;
                                break;
                        case KEY_UP:
                                move = SM_UP;
                                break;
                        case KEY_LEFT:
                                move = SM_LEFT;
                                break;
                        case KEY_DOWN:
                                move = SM_DOWN;
                                break;
                        /* Alternate directional input. */
                        case 'd':
                                move = SM_RIGHT;
                                break;
                        case 'w':
                                move = SM_UP;
                                break;
                        case 'a':
                                move = SM_LEFT;
                                break;
                        case 's':
                                move = SM_DOWN;
                                break;
                        default:
                                break;
                }
        }

        /* De-initialize curses. */
        de_init_curses();

        /* Clean up. */
        sg_delete(game);

        /* Exit with a success. */
        return 0;
}
