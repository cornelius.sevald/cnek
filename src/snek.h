/************************************************
 *
 * @file        snek.h
 *
 * @author      Cornelius Sevald-Krause
 *
 * @date        Created Tuesday, 26 February 2019
 *
 * @license     GNU GPLv3
 *
************************************************/

#ifndef SNEK_H
#define SNEK_H

#define SNEK_START_LEN 3
#define SNEK_START_DIR DIR_UP

/*
 * The different cell types.
 */
enum snake_cells {
        SC_EMPTY, SC_SEGMENT, SC_FRUIT
};

/*
 * The four different directions on the grid.
 */
enum directions {
        DIR_RIGHT, DIR_UP, DIR_LEFT, DIR_DOWN
};

/*
 * The different game states.
 */
enum game_state {
        GS_RUNNING, GS_LOST, GS_WON
};

/*
 * Different user inputs
 */
enum snek_move {
        SM_RIGHT, SM_UP, SM_LEFT, SM_DOWN, SM_NONE
};

/*
 * The different wall collision types.
 *  - WC_DIE kills the snake when colliding with the wall.
 *  - WC_TELEPORT teleports the snake to the opposite side of
 * the board.
 */
enum wall_collisions {
        WC_DIE, WC_TELEPORT
};

/*
 * The location in the game board.
 */
struct cell_location {
        int row;
        int col;
};

/*
 * A segment in the snake.
 */
struct snekment {
        struct cell_location loc;
        struct snekment *prev;
};

/*
 * A snake struct with a reference to the head of the snake.
 */
struct snek {
        enum directions dir;
        struct snekment *head;
        int grow_buffer;
};

/*
 * A fruit struct that spawns randomly and can be eaten by the snake.
 */
struct fruit {
        struct cell_location loc;
};

struct snek_game {
        /*
         * Board related fields.
         */
        int rows;
        int cols;

        /*
         * Scoring.
         */
        int fruit_score;
        int tick_score;

        /*
         * Game state.
         */
        enum game_state state;

        /*
         * Wall collision types.
         */
        enum wall_collisions wc_type;

        /*
         * Board objects
         */
        struct snek *snek;
        struct fruit *current_fruit;
};

/*
 * Game object manipulation.
 */
void sg_init (struct snek_game *gm_obj, int rows, int cols, enum wall_collisions wc_type);
struct snek_game *sg_create (int rows, int cols, enum wall_collisions wc_type);
void sg_destroy(struct snek_game *gm_obj);
void sg_delete(struct snek_game *gm_obj);

/*
 * Game-logic functions.
 */
void sg_tick (struct snek_game *game, enum snek_move move);

#endif // SNEK_H
