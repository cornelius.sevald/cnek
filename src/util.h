/************************************************
 *
 * @file        util.h
 *
 * @author      Cornelius Sevald-Krause
 *
 * @date        Created Thursday, 7 March 2019
 *
 * @license     GNU GPLv3
 *
************************************************/

#ifndef UTIL_H
#define UTIL_H

/*
 * Sleep for N milliseconds.
 * Written by Stephen Brennman.
 */
void sleep_milli (int milliseconds);

#endif // UTIL_H
