/************************************************
 *
 * @file        snek.c
 *
 * @author      Cornelius Sevald-Krause
 *
 * @date        Created Tuesday, 26 February 2019
 *
 * @license     GNU GPLv3
 *
************************************************/

#include <stdlib.h>
#include "snek.h"

/*
 * Snake object manipulation.
 */
static void snek_init (struct snek *snek_obj, int row, int col, int start_len, enum directions dir);
static struct snek *snek_create (int row, int col, int start_len, enum directions dir);
static void snek_destroy (struct snek *snek_obj);
static void snek_delete (struct snek *snek_obj);
/*
 * Fruit object manipulation.
 */
static struct fruit *fruit_create (int row, int col);
static void fruit_delete (struct fruit *fruit_obj);
/*
 * Game-logic helper functions.
 */
static enum directions opposite_dir (enum directions dir);
static struct cell_location get_move (struct cell_location loc, enum directions dir);
static unsigned long snek_len (struct snek *snek);
static int overlaps_snek (struct snek *snek, struct cell_location loc);
static int game_won (struct snek_game *game);
static struct fruit *fruit_spawn (struct snek_game *game);
static int snek_move (struct snek_game *game);
static void snek_eat (struct snek_game *game);

static void snek_init (struct snek *snek_obj, int row, int col, int start_len, enum directions dir) {
        /* Initialize the snake head. */
        struct snekment *head = malloc(sizeof(struct snekment));
        struct cell_location loc = {row, col};
        head->loc = loc;
        head->prev = NULL;

        snek_obj->dir = dir;
        snek_obj->head = head;
        snek_obj->grow_buffer = start_len - 1;
}

static struct snek *snek_create (int row, int col, int start_len, enum directions dir) {
        struct snek *snek_obj = malloc(sizeof(struct snek));
        snek_init(snek_obj, row, col, start_len, dir);
        return snek_obj;
}

static void snek_destroy (struct snek *snek_obj) {
        /* Clean up all of the snake segments */
        struct snekment *current = snek_obj->head;
        while (current) {
                struct snekment *old = current;
                current = current->prev;
                free(old);
        }
}

static void snek_delete (struct snek *snek_obj) {
        snek_destroy(snek_obj);
        free(snek_obj);
}

static void fruit_init (struct fruit *fruit_obj, int row, int col) {
        struct cell_location loc = {row, col};
        fruit_obj->loc = loc;
}

static struct fruit *fruit_create (int row, int col) {
        struct fruit *fruit_obj = malloc(sizeof(struct fruit));
        fruit_init(fruit_obj, row, col);
        return fruit_obj;
}

static void fruit_delete (struct fruit *fruit_obj) {
        free(fruit_obj);
}

static struct fruit *fruit_spawn (struct snek_game *game) {
        int rows = game->rows;
        int cols = game->cols;
        struct snek *snek = game->snek;
        /* Check if every cell is occupied by snake segments. */
        if (snek_len(snek) >= (unsigned long) rows * (unsigned long) cols) {
                return NULL;
        }

        /* Generate random coordinates for the fruit. */
        struct cell_location loc;
        do {
                loc.row = rand() % rows;
                loc.col = rand() % cols;
                /* Keep re-generating cordinates if the current overlap the snake. */
        } while (overlaps_snek(snek, loc));

        struct fruit *fruit = fruit_create(loc.row, loc.col);
        return fruit;
}

static enum directions opposite_dir (enum directions dir) {
        return (dir + 2) % 4;
}

static struct cell_location get_move (struct cell_location loc, enum directions dir) {
        switch (dir) {
                case DIR_RIGHT:
                        loc.col++; break;
                case DIR_UP:
                        loc.row++; break;
                case DIR_LEFT:
                        loc.col--; break;
                case DIR_DOWN:
                        loc.row--; break;
                default:
                        /* Given an invalid direction, do nothing to `loc`. */
                        break;
        }
        return loc;
}

static unsigned long snek_len (struct snek *snek) {
        unsigned long count = 0;

        /* Loop over all of the segments and count for each one */
        for (
                struct snekment *current = snek->head;
                current;
                current = current->prev
        ) {
                count++;
        }

        return count;
}

static int overlaps_snek (struct snek *snek, struct cell_location loc) {
        /* Loop over all of the segments and see if the coordinates are equal. */
        for (
                struct snekment *current = snek->head;
                current;
                current = current->prev
        ) {
                if (
                        current->loc.row == loc.row &&
                        current->loc.col == loc.col
                ) {
                        /* The cell has the same cooridnates as the segment. */
                        return 1;
                }
        }
        return 0;
}

static int game_won (struct snek_game *game) {
        struct snek *snek = game->snek;
        int rows = game->rows;
        int cols = game->cols;

        return snek_len(snek) >= (unsigned long) rows * (unsigned long) cols;
}

static int snek_move (struct snek_game *game) {
        struct snek *snek = game->snek;
        struct fruit *fruit = game->current_fruit;
        enum wall_collisions wc_type = game->wc_type;
        int rows = game->rows;
        int cols = game->cols;
        int eat = 0; // Did the snake collide with a fruit?

        /* If the snake has no segments, the move was failed. */
        if (!snek->head) {
                return 0;
        }

        struct cell_location new_loc = get_move(
                snek->head->loc,
                snek->dir
        );
        /* Check if new head location is out of bounds. */
        if (
                new_loc.row < 0 || new_loc.row >= rows ||
                new_loc.col < 0 || new_loc.col >= cols
        ) {
                if (wc_type == WC_DIE) {
                        /* Return 0 (false) to signify failure. */
                        return 0;
                } else if (wc_type == WC_TELEPORT) {
                        new_loc.row = (new_loc.row + rows) % rows;
                        new_loc.col = (new_loc.col + cols) % cols;
                }
        }
        /*
         * Check for self collision. Do not use `overlaps_snek` as collision with
         * the last segment should be ignore because it will move forward one cell.
         */
        for (
                struct snekment *current = snek->head;
                current->prev;
                current = current->prev
        ) {
                if (
                        new_loc.col == current->loc.col &&
                        new_loc.row == current->loc.row
                ) {
                        /* Self intersection with anything but the last piece results in faliure. */
                        return 0;
                }
        }
        /* Check if colliding with fruit. */
        if (
                new_loc.col == fruit->loc.col &&
                new_loc.row == fruit->loc.row
        ) {
                eat = 1;
        }

        /*
         * Do the actual moving of the snek segments.
         */
        struct snekment *current = snek->head;
        struct snekment *old;
        struct cell_location old_loc;
        do {
                old_loc = current->loc;
                current->loc = new_loc;

                old = current;
                current = current->prev;
                new_loc = old_loc;
        } while (current);

        /* If the snake has buffered growth, add a segment where the tail was. */
        if (snek->grow_buffer) {
                struct snekment *tail = malloc(sizeof(struct snekment));
                tail->prev = NULL;
                tail->loc = new_loc;

                old->prev = tail;
                snek->grow_buffer--;
        }

        // If the snake collided with a fruit, eat it.
        if (eat) {
                snek_eat(game);
        }

        /* Return 1 (true) after a successful move. */
        return 1;
}

static void snek_eat (struct snek_game *game) {
        game->snek->grow_buffer++;
        fruit_delete(game->current_fruit);
        game->current_fruit = fruit_spawn(game);

        game->fruit_score++;
}

void sg_init (struct snek_game *gm_obj, int rows, int cols, enum wall_collisions wc_type) {
        gm_obj->rows = rows;
        gm_obj->cols = cols;

        gm_obj->fruit_score = 0;
        gm_obj->tick_score = 0;

        gm_obj->state = GS_RUNNING;

        gm_obj->wc_type = wc_type;

        /* Create the snake at the center of the board facing up */
        gm_obj->snek = snek_create(rows / 2, cols / 2, SNEK_START_LEN, SNEK_START_DIR);
        gm_obj->current_fruit = fruit_spawn(gm_obj);
}

struct snek_game *sg_create (int rows, int cols, enum wall_collisions wc_type) {
        struct snek_game *gm_obj = malloc(sizeof(struct snek_game));
        sg_init (gm_obj, rows, cols, wc_type);
        return gm_obj;
}

void sg_destroy (struct snek_game *gm_obj) {
        /* Clean up allocated memory. */
        snek_delete(gm_obj->snek);
        fruit_delete(gm_obj->current_fruit);
}

void sg_delete (struct snek_game *gm_obj) {
        sg_destroy(gm_obj);
        free(gm_obj);
}

void sg_tick (struct snek_game *game, enum snek_move move) {
        if (game->state != GS_RUNNING) {
                return;
        }
        struct snek *snek = game->snek;
        enum directions new_dir = snek->dir;
        switch (move) {
                case SM_RIGHT:
                        new_dir = DIR_RIGHT;
                        break;
                case SM_UP:
                        new_dir = DIR_UP;
                        break;
                case SM_LEFT:
                        new_dir = DIR_LEFT;
                        break;
                case SM_DOWN:
                        new_dir = DIR_DOWN;
                        break;
                default:
                        break;
        }
        /* Change the direction if it is not the opposite. */
        if (new_dir != opposite_dir(snek->dir)) {
                snek->dir = new_dir;
        }

        /* Check if the game is already won. */
        if (game_won(game)) {
                game->state = GS_WON;
                return;
        }
        /* Move the snake and check if the move was not successful. */
        if (!snek_move(game)) {
                /* An unsuccessful move means the game is over. */
                game->state = GS_LOST;
                return;
        }
        game->tick_score++;
}
